# Exercice algo stage 1

## Consignes

**exercice 1**

Ecrire un algorithme qui demande successivement 20 nombres à  l’utilisateur, et qui lui dise ensuite quel était le plus grand parmi  ces 20 nombres. Modifiez ensuite l’algorithme pour que le programme affiche de surcroît en quelle position avait été saisie ce nombre

**exercice 2**

Ecrire un algorithme qui demande un nombre de départ, et qui ensuite  écrit la table de multiplication de ce nombre, présentée comme suit (cas où l'utilisateur entre le nombre 7) 

**exercice 3**

Ecrire un algorithme qui demande un nombre de départ, et qui calcule sa factorielle.

**exercice 4**

Écrire un algorithme qui permette de connaître ses chances de gagner au tiercé, quarté, quinté et autres impôts volontaires. On demande à l’utilisateur le nombre de chevaux partants, et le  nombre de chevaux joués. Les deux messages affichés devront être :

Dans l’ordre : une chance sur X de gagner Dans le désordre : une chance sur Y de gagner

X = n ! / (n - p) ! Y = n ! / (p ! * (n – p) !)

**exercice 5**

Faire un algorithme qui détermine la longueur d’une chaîne de caractères (sans .length)

**exercice 6**

Faire une fonction de concaténation (ajoute à la fin de la première  chaîne de caractères le contenu de la deuxième chaîne de caractères.)

Faire une fonction de Comparaison qui compare deux chaînes de caractères suivant l’ordre lexicographique.

Faire une fonction qui efface une partie de la chaîne en spécifiant  une longueur d’effacement et un indice à partir duquel il faut effacer.

**exercice 7**

Ecrire un algorithme qui affiche la moyenne d’une suite d’entiers

**exercice 8**

Ecrire une fonction max3 qui retourne le maximum de trois entiers
Ecrire une fonction min3 qui retourne le minimum de trois entiers
Ecrire une fonction max2 qui retourne le maximum de deux entiers
Ecrire une fonction max3 qui retourne le maximum de trois entiers en faisant appel à max2

**exercice 9**

Ecrire une action qui fournit les félicitations ou l’ajournement d’un élève suivant sa note en utilisant Si-alors-sinon



## Réalisation

Exercices réalisés : [ 1, 2, 3, 5, 7, 8, 9 ]

**Run un exercice**

```shell
npm run exo<numéro exercice>
```