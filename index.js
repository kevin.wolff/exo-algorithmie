const prompts = require('prompts');

async function prompter(type, message) {
    return await prompts({
        type: type,
        name: 'value',
        message: message,
        validate: value => value > 0 && value < 100
    })
}

// Exercice 1 : récupérer 10 nombres, afficher le plus grand et son index
module.exports.exo1 = async function exo1() {
    console.log('Exercice numéro 1 - plus grand et son index')
    console.log('Renseigner cinq chiffres/nombres')

    let answers = []
    while (answers.length < 5) {
        const answer = await prompter('number', 'Entier entre 1 et 99', )
        answers.push(answer.value)
    }

    let higherNumber = 0
    let higherNumberIndex = 0
    answers.forEach((answer, index) => {
        if (answer > higherNumber) {
            higherNumber = answer
            higherNumberIndex = index
        }
    })

    console.log(`Le plus grand : ${higherNumber}`)
    console.log(`Son index : ${higherNumberIndex}`)
}

// Exercice 2 : table de multiplication
module.exports.exo2 = async function exo2() {
    console.log('Exercice numéro 2 - table de multiplication')
    console.log('Renseigner un chiffre/nombre à multiplier')

    const table = await prompter('number','Entier entre 1 et 99')
    for (let i = 0; i <= 10; i++) {
        console.log(`${table.value} x ${i} = ${table.value * i}`)
    }
}

// Exercice 3 : nombre factoriel
module.exports.exo3 = async function exo3() {
    console.log('Exercice numéro 3 - nombre factoriel')
    console.log('Renseigner un chiffre/nombre à factoriser')

    const initialNumber = await prompter('number', 'Entier entre 1 et 99')

    let result = initialNumber.value
    for (let i = 1; i <initialNumber.value; i++) {
        result = result * i
    }

    console.log(`La valeur factorielle de ${initialNumber.value} est ${result}`)
}

// Exercice 5 : nombre de caractères
module.exports.exo5 = async function exo5() {
    console.log('Exercice numéro 5 - nombre de caractères')
    console.log('Renseigner une phrase pour y compter les caractères')

    const string = await prompts({
        type: 'text',
        name: 'value',
        message: 'Phrase',
    })

    let numberCharacters = 0

    const stringArrayWithoutSpace = string.value.split(' ')
    let stringWithoutSpace = ''
    stringArrayWithoutSpace.forEach((word) => {
        stringWithoutSpace = stringWithoutSpace + word
    })

    const charactersArray = stringWithoutSpace.split('')
    charactersArray.forEach(() => {
        numberCharacters ++
    })

    console.log(`${numberCharacters} caractères dans cette phrase`)
}

// Exercice 7 : moyenne
module.exports.exo7 = async function exo7() {
    console.log('Exercice numéro 7 - moyenne')
    console.log('Renseigner des chiffres/nombres pour calculer leur moyenne')

    const numberOfInteger = await prompter('number', 'Nombre de valeurs dont on cherche la moyenne')

    const integers = []
    for (let i = 0; i < numberOfInteger.value; i++) {
        let integer = await prompter('number', 'Entier entre 1 et 99')
        integers.push(integer.value)
    }

    let totalIntegers = 0
    integers.forEach(integer => {
        totalIntegers = totalIntegers + integer
    })

    const integersEverage = (totalIntegers / numberOfInteger.value).toFixed(2)

    console.log(`La moyenne des valeurs indiquées est ${integersEverage}`)
}

// Exercice 8 : max3 et max2
/*module.exports.exo8 = async function exo8() {
    console.log('Exercice numéro 8 - max3 et max2')

    const numbers = []
    for (let i = 0; i < 3; i ++) {
        let number = await prompter('number', 'Un nombre entier entre 1 et 99')
        numbers.push(number.value)
    }
}*/

// Exercice 9 : if elseif else
module.exports.exo9 = async function exo9() {
    console.log('Exercice numéro 9 - if ifelse else')
    console.log('Renseigner une note pour savoir si elle est recevable ou non')

    const note = await prompter('number', 'Note de l\'eleve')

    console.log(note.value >= 10 ? 'Eleve reçus' : 'Eleve refusé')
}
